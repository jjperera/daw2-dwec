"use strict";

window.addEventListener('load', () => {

    document.getElementById('sumar').addEventListener('click', sumar);

});

function sumar(evento) {

    let resultado = 0;
    let campos = document.querySelectorAll('.campo');
    for(let c of campos) {
        
       resultado += Number(c.value); 
        c.value=0;
    } 
    
    document.getElementById('resultado').value = resultado;
}