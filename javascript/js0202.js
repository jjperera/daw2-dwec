//----------------------------------------------------------
// Sumar elementos
//----------------------------------------------------------
"use strict;"
const prompt = require('prompt-sync')();

// Declaro un array vacío
let array = [];

// Inserta elementos en el array en el orden de lectura utilizando push
let fin = false;
while(!fin) {

    let cadena = prompt("Introduce un número : ");
    
    if(cadena.length == 0) {

        // He terminado con la lectura
        fin = true;

    } else if(!Number.isNaN(cadena)) {
        
        // Si es un número válido lo añade al array. Hace conversón explícita.
        // en caso contrario no se podrían sumar.
        array.push( Number(cadena) );

    } else {

        // Si no es un número lo indica
        console.log("El número no es válido");
    }
}

// Longitud
console.log("La longitud del array es : "+array.length);

// Muestra los elementos
console.log("Números en orden");
for(let numero of array) {
    console.log(numero);
}

// Muestra los elementos en orden inverso
console.log("Números en orden inverso");
for(let numero of array.reverse()) {
    console.log(numero);
}

// Calcula la suma de los elementos
console.log("Suma de los elementos");
let suma = 0;
for(let numero of array) {
    suma += numero
}
console.log("Suma = "+suma);
