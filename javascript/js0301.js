//----------------------------------------------------------
// Factorial
//----------------------------------------------------------
"use strict;"
const prompt = require('prompt-sync')();

function factorial(x) {
    return x > 1 ? factorial(x-1)*x : 1;
}

console.log(factorial(3));
console.log(factorial(5));
