//----------------------------------------------------------
// Calculadora. Object.create
//----------------------------------------------------------
"use strict;"
const prompt = require('prompt-sync')();

// Constantes necesarias
const OPERACIONES_PERMITIDAS = "+-*/CMRq";
const OPERACIONES_UNARIAS = "CMRq";


// Crea el objeto heredando de Object
let calculadora = Object.create(Object.prototype);

// Ahora asigna propiedades y métodos
calculadora.pantalla  = 0;
calculadora.memoria = 0;
calculadora["+"] = function (x) { this.pantalla += x; };
calculadora["-"] = function (x) { this.pantalla -= x; };
calculadora["*"] = function (x) { this.pantalla *= x; };
calculadora["/"] = function (x) { this.pantalla /= x; };
calculadora["M"] = function () { this.memoria = this.pantalla; };
calculadora["R"] = function () { this.pantalla = this.memoria; };
calculadora["C"] = function () { 
    this.pantalla = 0;
    this.memoria = 0;
}


// Menú principal
function menuCalculadora() {

    let fin = false;
    while(!fin) {
    
        console.log("pantalla = "+calculadora.pantalla);queueMicrotask
        console.log("memoria = "+calculadora.memoria);
    
        // Cuando se detecte algún error se va a lanzar una excepción
        try {
            // Lee la entrada que necesita para la operación.
            let operacion = prompt("Operación ( -, +, *, /, C, M, R, q ) :");    
            if(OPERACIONES_PERMITIDAS.indexOf(operacion) < 0) {
                throw "Operación no soportada : "+operacion;
            }
    
            // Si se trata de una operación unaria la ejecuta
            if(OPERACIONES_UNARIAS.indexOf(operacion) >= 0) {

                if(operacion == "q") {
                    fin = true;
                } else {

                    // Si se trata de una operación, la ejecuto
                    calculadora[operacion]();
                }
    
            } else {
    
                // Lee el segundo operando 
                let operando = prompt("Operando : ");
                
                if(Number.isNaN(operando)) {
                    throw "El número introducido no es válido.";
                }
    
                // Conversión explícita a número
                operando = Number(operando);
    
                // Ejecuta la operación
                calculadora[operacion](operando);
            }
    
        } catch(error) {
            console.log("ERROR : "+error);
        }    
    }
}

menuCalculadora();

