// Haz un programa que lea un número mayor que cero por pantalla. 
// Si el número es incorrecto, deberá mostrar el error. 
// En caso contrario, imprimirá todos los números de 0 hasta el número indicado.
"use strict;"
const prompt = require('prompt-sync')();

// La variable número
let numero = NaN;   // Inicializo a un valor inválido

// Lee un número mayor que 0 por pantalla
let fin = false;
while(!fin) {
    
    numero = prompt("Introduce un número mayor que 0 : ");    

    // Comprueba que el valor introducido sea un número válido y que sea
    // > 0
    if(!Number.isNaN(numero) && numero > 0) {
        fin = true;
    } else {
        console.log("El valor introducido no es un número > 0");
    }
}

// Muestra todos los números desde 0 al valor indicado
for(let n = 0;n <= numero;n++) {
    console.log(n);
}