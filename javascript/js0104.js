//----------------------------------------------------------
// Adivina el número
//----------------------------------------------------------
"use strict;"
const prompt = require('prompt-sync')();

/** Número de intentos disponibles */
const MAX_NUMERO_INTENTOS = 10;

// Genera un número aleatorio entre 1 y 100.
let secreto = 1 + Math.random()*99;

// Elimina los decimales del número
secreto = secreto.toFixed();

// Asigna los intentos restantes
let ganado = false;
let intentosRestantes = MAX_NUMERO_INTENTOS;

// Repito mientras que la partida no haya terminado y tenga intentos restantes
while(!ganado && intentosRestantes > 0) {

    let numero = prompt("Introduce un número entre 1 y 100 : ");

    if(numero == secreto) {

        console.log("Has ganado !!!");
        ganado = true;

    } else {

        // Verifica si el número es mayor o menor
        if(numero < secreto) {

            console.log("MENOR");

        } else if(numero > secreto) {

            console.log("MAYOR");
        }

        // Decremento los intentos restantes
        intentosRestantes--;

    }
}

