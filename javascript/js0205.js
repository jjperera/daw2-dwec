//----------------------------------------------------------
// Lista de la compra
//----------------------------------------------------------
"use strict;"
const prompt = require('prompt-sync')();

// Declaro dla lista de la compra
let listaCompra = new Set();

let fin = false;
while(!fin) {

    let entrada = prompt("Introduce entrada : ");

    if(entrada.length == 0) {

        // Si la longitud es 0, hemos terminado
        fin = true;
    } else {

        if(listaCompra.has(entrada)) {
            console.log("El elemento ya está en la lista");
        } else {
            listaCompra.add(entrada);
        }
    }
}

// Muestra el contenido de la lista
let listaOrdenada = Array.from(listaCompra.values()).sort();
for(let e of listaOrdenada) {
    console.log(e);
}